import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JSlider;
import javax.swing.JComboBox;
import java.awt.Checkbox;
import javax.swing.JRadioButton;
import javax.swing.JButton;
import javax.swing.JMenuBar;
import java.awt.TextField;
import javax.swing.JScrollBar;
import javax.swing.JLabel;
import javax.swing.DefaultComboBoxModel;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JPasswordField;
import java.awt.Component;
import javax.swing.Box;
import java.awt.Choice;
import javax.swing.JProgressBar;
import javax.swing.JMenuItem;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.JMenu;
import javax.swing.ButtonGroup;
import java.awt.Color;
import java.awt.Font;
import javax.swing.JSeparator;
import java.awt.Canvas;
import javax.swing.JTable;
import java.awt.Toolkit;

public class VentanaEclipse extends JFrame {

	private JPanel contentPane;
	private JPasswordField passwordField;
	private final ButtonGroup buttonGroup = new ButtonGroup();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					VentanaEclipse frame = new VentanaEclipse();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public VentanaEclipse() {
		setIconImage(Toolkit.getDefaultToolkit().getImage("C:\\entornos-workspace\\icono.ico"));
		setTitle("BOOMIC");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 698, 496);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(176, 224, 230));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setFont(new Font("Tahoma", Font.PLAIN, 12));
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"Elige una opci\u00F3n", "Mujer", "Hombre", "No especificar"}));
		comboBox.setToolTipText("");
		comboBox.setBounds(97, 371, 142, 23);
		contentPane.add(comboBox);
		
		Checkbox checkbox_1 = new Checkbox("Acepto los t\u00E9rminos y condiciones");
		checkbox_1.setBounds(382, 259, 241, 22);
		contentPane.add(checkbox_1);
		
		JRadioButton rdbtnNewRadioButton = new JRadioButton("Recibir notificaciones");
		rdbtnNewRadioButton.setFont(new Font("Gadugi", Font.PLAIN, 13));
		rdbtnNewRadioButton.setBackground(new Color(176, 224, 230));
		buttonGroup.add(rdbtnNewRadioButton);
		rdbtnNewRadioButton.setBounds(382, 204, 165, 23);
		contentPane.add(rdbtnNewRadioButton);
		
		JRadioButton rdbtnNewRadioButton_2 = new JRadioButton("No recibir notificaciones");
		rdbtnNewRadioButton_2.setFont(new Font("Gadugi", Font.PLAIN, 13));
		rdbtnNewRadioButton_2.setBackground(new Color(176, 224, 230));
		buttonGroup.add(rdbtnNewRadioButton_2);
		rdbtnNewRadioButton_2.setBounds(382, 230, 207, 23);
		contentPane.add(rdbtnNewRadioButton_2);
		
		JButton btnNewButton = new JButton("Aceptar");
		btnNewButton.setBackground(new Color(255, 255, 255));
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btnNewButton.setBounds(429, 298, 88, 23);
		contentPane.add(btnNewButton);
		
		JMenuBar menuBar = new JMenuBar();
		menuBar.setBounds(0, 0, 682, 21);
		contentPane.add(menuBar);
		
		JMenu mnAtrs = new JMenu("Atr\u00E1s");
		mnAtrs.setFont(new Font("Segoe UI", Font.BOLD, 12));
		menuBar.add(mnAtrs);
		
		JMenu mnReiniciar = new JMenu("Reiniciar");
		mnReiniciar.setFont(new Font("Segoe UI", Font.BOLD, 12));
		menuBar.add(mnReiniciar);
		
		JMenu mnSalir = new JMenu("Salir");
		mnSalir.setFont(new Font("Segoe UI", Font.BOLD, 12));
		menuBar.add(mnSalir);
		
		JMenu mnSiguiente = new JMenu("Siguiente");
		mnSiguiente.setFont(new Font("Segoe UI", Font.BOLD, 12));
		menuBar.add(mnSiguiente);
		
		JLabel lblNombre = new JLabel("Nombre");
		lblNombre.setFont(new Font("Gadugi", Font.PLAIN, 15));
		lblNombre.setBounds(97, 111, 76, 14);
		contentPane.add(lblNombre);
		
		JLabel lblApellidos = new JLabel("Apellidos");
		lblApellidos.setFont(new Font("Gadugi", Font.PLAIN, 15));
		lblApellidos.setBounds(97, 174, 76, 14);
		contentPane.add(lblApellidos);
		
		JLabel lblContrasea = new JLabel("Contrase\u00F1a");
		lblContrasea.setFont(new Font("Gadugi", Font.PLAIN, 15));
		lblContrasea.setBounds(97, 299, 113, 14);
		contentPane.add(lblContrasea);
		
		JLabel lblCorreoElectrnico = new JLabel("Correo Electr\u00F3nico");
		lblCorreoElectrnico.setFont(new Font("Gadugi", Font.PLAIN, 15));
		lblCorreoElectrnico.setBounds(97, 238, 165, 14);
		contentPane.add(lblCorreoElectrnico);
		
		TextField textField = new TextField();
		textField.setBounds(97, 135, 142, 23);
		contentPane.add(textField);
		
		TextField textField_1 = new TextField();
		textField_1.setBounds(97, 200, 142, 23);
		contentPane.add(textField_1);
		
		TextField textField_3 = new TextField();
		textField_3.setBounds(97, 258, 143, 23);
		contentPane.add(textField_3);
		
		passwordField = new JPasswordField();
		passwordField.setBounds(97, 325, 142, 22);
		contentPane.add(passwordField);
		
		Component horizontalGlue = Box.createHorizontalGlue();
		horizontalGlue.setBounds(195, 168, 1, 1);
		contentPane.add(horizontalGlue);
		
		JProgressBar progressBar = new JProgressBar();
		progressBar.setToolTipText("");
		progressBar.setBackground(new Color(255, 255, 255));
		progressBar.setForeground(new Color(153, 255, 153));
		progressBar.setValue(60);
		progressBar.setBounds(372, 378, 217, 34);
		contentPane.add(progressBar);
		
		JLabel lblNewLabel = new JLabel("Progreso de registro");
		lblNewLabel.setFont(new Font("Gadugi", Font.PLAIN, 13));
		lblNewLabel.setBounds(421, 353, 136, 14);
		contentPane.add(lblNewLabel);
		
		JSeparator separator = new JSeparator();
		separator.setBounds(331, 341, 292, 1);
		contentPane.add(separator);
		
		JSlider slider = new JSlider();
		slider.setBackground(new Color(176, 224, 230));
		slider.setBounds(372, 135, 200, 23);
		contentPane.add(slider);
		
		JLabel lblRegistro = new JLabel("Registro");
		lblRegistro.setFont(new Font("Gisha", Font.BOLD, 17));
		lblRegistro.setBounds(307, 43, 217, 24);
		contentPane.add(lblRegistro);
		
		JLabel lblNivelDeRecomendaciones = new JLabel("Nivel de recomendaciones");
		lblNivelDeRecomendaciones.setFont(new Font("Gadugi", Font.PLAIN, 12));
		lblNivelDeRecomendaciones.setBounds(402, 113, 161, 14);
		contentPane.add(lblNivelDeRecomendaciones);
		
		JLabel lblNinguna = new JLabel("Ninguna");
		lblNinguna.setFont(new Font("Gadugi", Font.PLAIN, 12));
		lblNinguna.setBounds(344, 155, 67, 14);
		contentPane.add(lblNinguna);
		
		JLabel lblRecibirRecomendaciones = new JLabel("Muchas");
		lblRecibirRecomendaciones.setFont(new Font("Gadugi", Font.PLAIN, 12));
		lblRecibirRecomendaciones.setBounds(558, 155, 88, 14);
		contentPane.add(lblRecibirRecomendaciones);
	}
}
