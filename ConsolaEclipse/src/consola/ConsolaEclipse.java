package consola;
import java.util.Scanner;

import libreria.Bucles;
import libreria.Vectores;

public class ConsolaEclipse{
	public static void main (String[] args){
		Scanner teclado = new Scanner(System.in);
		System.out.println("MEN�:");
		System.out.println("1- Rellenar matriz y mostrarla por pantalla");
		System.out.println("2- Rellenar vector y decir si se repiten los valores");
		System.out.println("3- Mostrar longitud matriz");
		System.out.println("4- Ordenar vector");
		System.out.println("5- Salir");
		int[][]matriz = null;
		int[] array = new int[5];
		int opcion;
		do{
			System.out.print("Elige una opci�n: ");
			opcion = teclado.nextInt();
			switch(opcion){
			case 1:
				System.out.print("Introduce n�mero de filas: ");
				int filas = teclado.nextInt();
				System.out.print("Introduce n�mero de columnas: ");
				int columnas = teclado.nextInt();
				System.out.println("");
				matriz = Bucles.rellenarMatriz(teclado, filas, columnas);
				Bucles.mostrarMatriz(matriz);
				break;
			case 2:
				Bucles.rellenarArray(array, teclado);
				Bucles.valoresRepetidos(array);
				break;
			case 3:
				if(matriz!=null){
					Vectores.longitudMatriz(matriz);
				}else{
					System.out.println("No se ha rellenado ninguna matriz.");
				}
				break;
			case 4:
				Vectores.ordenarArray(array);
				break;
			case 5:
				break;
			default:
				System.out.println("No has introducido una opci�n v�lida.");
			}
			System.out.println("");
		}while(opcion!=5);
		
		teclado.close();
	}
}
