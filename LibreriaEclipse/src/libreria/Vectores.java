package libreria;

import java.util.Arrays;

public class Vectores{
	public static void longitudMatriz(int[][] matriz){
		System.out.println("Columnas: " + matriz[0].length);
		System.out.println("Filas: " + matriz.length);
	}
	
	public static int[] ordenarArray(int[] array){
		Arrays.sort(array);
		for (int i=0; i<array.length; i++){
			System.out.print(array[i] + " ");
		}
		
		return array;
	}
}
