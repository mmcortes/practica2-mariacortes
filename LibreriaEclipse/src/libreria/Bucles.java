package libreria;

import java.util.Scanner;

public class Bucles{
	public static int[][] rellenarMatriz(Scanner teclado, int filas, int columnas){
		int[][] matriz = new int[filas][columnas];
		for(int i=0; i<matriz.length; i++){
			for(int j=0; j<matriz[i].length; j++){
				System.out.print("Introduce un entero: ");
				matriz[i][j] = teclado.nextInt();
			}
		}
		
		return matriz;
	}
	
	public static void mostrarMatriz(int[][] matriz){
		for(int i=0; i<matriz.length; i++){
			for(int j=0; j<matriz[i].length; j++){
				System.out.print(matriz[i][j] + " ");
			}
			System.out.println("");
		}
	}
	
	public static int[] rellenarArray(int[] array, Scanner teclado){
		for(int i=0; i<array.length; i++){
			System.out.print("Introduce un entero: ");
			array[i] = teclado.nextInt();
		}
		
		return array;
	}
	
	public static void valoresRepetidos(int[] array){
		boolean iguales = false;
		for(int i=0; i<array.length; i++){
			for(int j=0; j<array.length; j++){
				if(array[i]==array[j] && i!=j){
					iguales = true;
				}
			}
		}
		if(iguales){
			System.out.println("Hay valores repetidos.");
		}else{
			System.out.println("No hay valores repetidos.");
		}
	}
}
