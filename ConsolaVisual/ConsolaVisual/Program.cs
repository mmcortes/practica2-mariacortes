﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsolaVisual
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("MENÚ:");
            Console.WriteLine("1- Rellenar matriz y mostrarla por pantalla");
            Console.WriteLine("2- Rellenar vector y decir si se repiten los valores");
            Console.WriteLine("3- Mostrar longitud matriz");
            Console.WriteLine("4- Ordenar vector");
            Console.WriteLine("5- Salir");
            int[,] matriz = null;
            int[] array = new int[5];
            int opcion;
            int columnas = 0;
            int filas = 0;
            do
            {
                Console.Write("Elige una opción: ");
                opcion = int.Parse(Console.ReadLine());
                switch (opcion)
                {
                    case 1:
                        Console.Write("Introduce número de filas: ");
                        filas = int.Parse(Console.ReadLine());
                        Console.Write("Introduce número de columnas: ");
                        columnas = int.Parse(Console.ReadLine());
                        Console.WriteLine("");
                        matriz = LIbreriaVisual.BuclesArrays.rellenarMatriz(filas, columnas);
                        LIbreriaVisual.BuclesArrays.mostrarMatriz(matriz, filas, columnas);
                        break;
                    case 2:
                        LIbreriaVisual.BuclesArrays.rellenarArray(array);
                        LIbreriaVisual.BuclesArrays.valoresRepetidos(array);
                        break;
                    case 3:
                        if (matriz != null)
                        {
                            LIbreriaVisual.BuclesArrays.longitudMatriz(filas, columnas);
                        }
                        else
                        {
                            Console.WriteLine("No se ha rellenado ninguna matriz.");
                        }
                        break;
                    case 4:
                        LIbreriaVisual.BuclesArrays.ordenarArray(array);
                        break;
                    case 5:
                        break;
                    default:
                        Console.WriteLine("No has introducido una opción válida.");
                        break;
                }
                Console.WriteLine("");
            } while (opcion != 5);
        }
    }
}