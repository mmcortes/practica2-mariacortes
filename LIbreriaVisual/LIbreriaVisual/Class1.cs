﻿using System;

namespace LIbreriaVisual
{
    public class BuclesArrays
    {
        public static int[,] rellenarMatriz(int filas, int columnas)
        {
            int[,] matriz = new int[filas, columnas];
            for (int i = 0; i < filas; i++)
            {
                for (int j = 0; j < columnas; j++)
                {
                    Console.Write("Introduce un entero: ");
                    matriz[i, j] = int.Parse(Console.ReadLine());
                }
            }

            return matriz;
        }

        public static void mostrarMatriz(int[,] matriz, int filas, int columnas)
        {
            for (int i = 0; i < filas; i++)
            {
                for (int j = 0; j < columnas; j++)
                {
                    Console.Write(matriz[i, j] + " ");
                }
                Console.WriteLine("");
            }
        }

        public static int[] rellenarArray(int[] array)
        {
            for (int i = 0; i < array.Length; i++)
            {
                Console.Write("Introduce un entero: ");
                array[i] = int.Parse(Console.ReadLine());
            }

            return array;
        }

        public static void valoresRepetidos(int[] array)
        {
            Boolean iguales = false;
            for (int i = 0; i < array.Length; i++)
            {
                for (int j = 0; j < array.Length; j++)
                {
                    if (array[i] == array[j] && i != j)
                    {
                        iguales = true;
                    }
                }
            }
            if (iguales)
            {
                Console.WriteLine("Hay valores repetidos.");
            }
            else
            {
                Console.WriteLine("No hay valores repetidos.");
            }
        }

        public static void longitudMatriz(int filas, int columnas)
        {
            Console.WriteLine("Columnas: " + columnas);
            Console.WriteLine("Filas: " + filas);
        }

        public static int[] ordenarArray(int[] array)
        {
            Array.Sort(array);
            for (int i = 0; i < array.Length; i++)
            {
                Console.Write(array[i] + " ");
            }

            return array;
        }
    }
}